#!/bin/bash
# Copyright (c) 2021-2022 GPLv3

#===========================
# Options for the script
#===========================

# Variable used to delete old log entries, 0 means no deletion
delete_log_entries_after_days=365

delete_log_files_after_days=15

# Set how much log entries are created.
# 0 = no logs
# 1 = error messages
# 2 = warn messages
# 3 = all messages
logging_level=0

# The name of the file that will be created in the end and is served
# by the webserver to the users.
file_name_state_site=state-site.html

#===========================
# End of options for the script
#===========================

current_time_in_ms=$(date '+%s%3N')
current_time=$(date +"%H:%M:%S")
current_date=$(date "+%m-%d-%Y")
delete_log_entries_after_milliseconds=$(echo "$delete_log_entries_after_days * 86400000" | bc)

if ! [ -f "$file_name_state_site" ]; then
    cp state-template.html $file_name_state_site
fi

log(){
	local line=$1
	local message=$2
	local log_folder=$3
	if [[ "$log_folder" == "" ]]; then
		return
	fi
	local time=$(date +"%H:%M:%S")
	echo "[$time] [line $line]: $message" >> "log/debug/$log_folder/$current_date.log"
}

log_info(){
	if [[ $logging_level -ge 3 ]]; then
		local line=$1
		local message=$2
		log "$line" "$message" "info"
	fi
}

log_warn(){
	if [[ $logging_level -ge 2 ]]; then
		local line=$1
		local message=$2
		log "$line" "$message" "warn"
	fi
}

log_error(){
	if [[ $logging_level -ge 1 ]]; then
		local line=$1
		local message=$2
		log "$line" "$message" "error"
	fi
}

# Calculates for how long a service was online in a time frame in percentage.
# time_of_period: Time (in milliseconds) of a day/week/...
# measured_online_time: Time (in milliseconds) in which a service was online.
# time_since_start: Time (in milliseconds) in which the state of the services is measured.
calculate_online_time(){
	local time_of_period=$1
	local measured_online_time=$2
	local time_since_start=$3
	if [[ $measured_online_time -eq 0 ]] || [[ $time_since_start -eq 0 ]]; then
		log_error ${LINENO} "Tried to divide through zero."
		return 1
	fi
	if [[ $time_since_start -gt $time_of_period ]]; then
		echo $(echo "scale=2; $measured_online_time*100/$time_of_period" | bc)
	elif [[ $time_since_start -ne 0 ]]; then
		echo $(echo "scale=2; $measured_online_time*100/$time_since_start" | bc)
	fi
}

# This function creates HTML elements which visualize for how much time a service was online
create_html_element_for_uptime(){
	local percentage=$1
	local kind_of_service=$2
	local service=$3
	local server=$4
	local site=$5
	local offline_percentage=$(echo "scale=2; 100 - $percentage" | bc)
	if (( $(echo "$offline_percentage > 100" | bc) )); then
		log_error ${LINENO} "Offline time more than 100%"
		return 1
	fi
	if [[ "$percentage" == "" ]]; then
		log_error ${LINENO} "Percentage is empty string."
		return 1
	fi
	local element="<div class=\"uptime-bar\"><div class=\"bar-left\" style=\"width: $percentage%\"></div><div class=\"bar-right\" style=\"width: $offline_percentage%\"></div><div class=\"bar-percentage\">$percentage%</div>"
	if [[ "$kind_of_service" == "server" ]]; then
		element+="<div class=\"icon-with-description\"><div class=\"icon-container\">"
		if [[ "$service" =~ "unencrypted" ]]; then
			element+="<img class=\"icon\" src=\"images/lock_open_icon.svg\" alt=\"Icon: Unverschlüsselt\" title=\"unverschlüsselt\"></div><div class=\"icon-description-container\"><div class=\"bar-si\">$server</div><div class=\"icon-description\">Unverschlüsseltes DNS über UDP</div>"
		else
			element+="<img class=\"icon\" src=\"images/lock_icon.svg\" alt=\"Icon: Verschlüsselt\" title=\"verschlüsselt\"></div><div class=\"icon-description-container\"><div class=\"bar-si\">$server</div><div class=\"icon-description\">Verschlüsseltes DNS über TLS</div>"
		fi
		element+="</div></div>"
	else
		element+="<div class=\"bar-site\">$site</div>"
	fi
	element+="</div>"
	if [[ "$percentage" == "" ]]; then
		element="<div>Keine Daten verfügbar!</div>"
	fi
	echo "$element"
}

# Two time frames are given. One is a time frame like the last day, week, month, or a year. The second
# one is a time frame in which a service was online.
# An example: One server was online for the last 8 days (first frame). Now we want to find out how long
# the service was online in the last week (second frame). Obviously the server was online for 7 days.
# So we cut off the day before the last 7 days.
# If the server is only online since the day before yesterday until a few minutes ago we have to
# calculate the duration.
#
# Output: Time in milliseconds
calculate_online_time_in_time_frame(){
	#First frame is any time frame in which the service is online
	local first_frame_start=$1
	local first_frame_end=$2
	#Second frame is a frame like for example a month or a year
	local second_frame_beginning=$3

	if [[ $first_frame_start -lt 0 ]] || [[ $first_frame_end -lt 0 ]] || [[ second_frame_beginning -lt 0 ]]; then
		log_error ${LINENO} "At least one value below zero."
		return -1
	elif (( $(echo "$first_frame_end < $first_frame_start" | bc) )); then
		log_error ${LINENO} "End of time frame is an earlier point than start of time frame."
		return -1
	fi

	if [[ first_frame_end -gt second_frame_beginning ]]; then
		local online_time=$(( first_frame_end - first_frame_start))
		local time_before_second_frame=0
		#Dismiss the time before the time frame that interests us
		if [[ first_frame_start -lt second_frame_beginning ]]; then
			time_before_second_frame=$(( second_frame_beginning - first_frame_start ))
		fi
		local added_time=$(( online_time - time_before_second_frame ))
		echo $added_time
	else
		echo 0
	fi
}

create_dir(){
	if ! [[ -d "$1" ]]; then
		mkdir --parents "$1"
	fi
}

create_website_state_html_elements() {
	local service=$1
	local current_state=$2
	if [[ "$service" == "" ]] || [[ "$current_state" == "" ]]; then
		return 1
	fi
	# Fill information into HTML elements
	element="<div class=\"site-state-entry $current_state\">\n
		<a href=\"$service\">$service</a>\n"
	if [[ "$current_state" == "online" ]]; then
		element+="<div class=\"online-text\">OK</div>\n"
	else
		element+="<div class=\"offline-text\">Offline</div>\n"
	fi
}

create_server_state_html_elements(){
	local server_name="$1"
	local state="$2"
	if [[ "$server_name" == "" ]] || [[ "$state" == "" ]]; then
		return 1
	fi
	encrypted="$3"
	if [[ $encrypted != "" ]]; then
		encrypted="-$encrypted"
	fi

	element="<div class=\"part-of-state $state\">"
}

delete_old_logs(){
	local path="$1"
	local origin=$(pwd)
	if [[ ! -d $path ]]; then
		return
	fi
	cd $path
	declare -a files_to_delete
	for file in *; do
		if [[ $file == "*" ]]; then continue; fi
		readarray -d . -t strings <<< "$file"
		readarray -d - -t date_array <<< "${strings[0]}"
		date_of_file=$(date -d "${date_array[0]}/${date_array[1]}/${date_array[2]}" +%s%3N)
		if [[ $date_of_file -lt $(echo "$current_time_in_ms - $delete_log_files_after_days * 86400000" | bc) ]]; then
			files_to_delete+=($file)
		fi
	done
	for file in ${files_to_delete[*]}; do
		rm $file
	done
	cd $origin
}

while getopts "d:" option; do
	case $option in
		d)
			number_regex='^[0-9]+$'
			if [[ "$OPTARG" =~ $number_regex ]]; then
				logging_level=$OPTARG
			else
				echo "Please enter a number for option d."
				log_error ${LINENO} "Please enter a number for option d."
			fi
	esac
done

time_overall=0

sites_to_check_with_dns_server=( example.org google.com microsoft.com ix.de youtube.com )
dns_servers=( dns2.digitalcourage.de dns3.digitalcourage.de )
sites_to_check=( https://digitalcourage.de/ https://bigbrotherawards.de/ https://digitalcourage.social/ https://digitalcourage.video/ https://nuudel.digitalcourage.de/ https://pad.foebud.org/ https://cryptpad.digitalcourage.de/ https://gitlab.digitalcourage.de/ )

cp state-template.html state-site-new.html

create_dir tmp
create_dir log
create_dir log/time-of-first-measurement

error_dir="log/debug/error"
warn_dir="log/debug/warn"
info_dir="log/debug/info"

delete_old_logs $error_dir
delete_old_logs $warn_dir
delete_old_logs $info_dir

if [[ $logging_level -ge 1 ]]; then
	create_dir $error_dir
fi
if [[ $logging_level -ge 2 ]]; then
	create_dir $warn_dir
fi
if [[ $logging_level -ge 3 ]]; then
	create_dir $info_dir
fi


### Testing the uptime and speed of the dns servers ###

for server in ${dns_servers[*]}; do
	echo 0 > "tmp/$server-normal-errors.tmp"
	echo 0 > "tmp/$server-encrypted-errors.tmp"
	echo 0 > "tmp/$server-normal-total-time.tmp"
	echo 0 > "tmp/$server-encrypted-total-time.tmp"
	echo "Start test for server $server"
	echo

	for site in ${sites_to_check_with_dns_server[*]}; do
		#test with DoT
		(
		echo $(date '+%s%3N')  > "tmp/$server-start-time-encrypted.tmp"
		kdig "$site" @"$server" -p 853 +tls >/dev/null
		exit_code=$?
		echo $(date '+%s%3N')  > "tmp/$server-end-time-encrypted.tmp"

		if [[ "$exit_code" != "0" ]]; then
			echo "Error resolving: $site"
			errors="$( cat "tmp/$server-encrypted-errors.tmp" )"
			((errors++))
			echo "$errors" > "tmp/$server-encrypted-errors.tmp"
		fi

		echo "Server $server"
		echo "Site $site"
		echo encrypted
		echo $(( "$( cat "tmp/$server-end-time-encrypted.tmp" )" - "$( cat "tmp/$server-start-time-encrypted.tmp" )" + "$( cat "tmp/$server-encrypted-total-time.tmp" )")) > "tmp/$server-encrypted-total-time.tmp"
		echo Time $(( "$( cat "tmp/$server-end-time-encrypted.tmp" )" - "$( cat "tmp/$server-start-time-encrypted.tmp" )" ))
		echo "Total time $( cat "tmp/$server-encrypted-total-time.tmp" ) ms"
		echo
		) &

		sleep 0.5

		#test without encryption
		if [ "$server" != dns3.digitalcourage.de ]; then
				(
				echo $(date '+%s%3N') > "tmp/$server-start-time-normal.tmp"
				kdig "$site" @"$server" -p 53 +notls +notcp >/dev/null
				exit_code=$?
				echo $(date '+%s%3N') > "tmp/$server-end-time-normal.tmp"

				if [[ "$exit_code" != "0" ]]; then
						echo "Error resolving: $site"
						errors="$( cat "tmp/$server-normal-errors.tmp" )"
						((errors++))
						echo "$errors" > "tmp/$server-normal-errors.tmp"
				fi

				echo "Server $server"
				echo "Site $site"
				echo unencrypted
				echo $(( "$( cat "tmp/$server-end-time-normal.tmp" )" - "$( cat "tmp/$server-start-time-normal.tmp" )" + "$( cat "tmp/$server-normal-total-time.tmp" )")) > "tmp/$server-normal-total-time.tmp"
				echo Time $(( "$( cat "tmp/$server-end-time-normal.tmp" )" - "$( cat "tmp/$server-start-time-normal.tmp" )" ))
				echo "Total time $( cat "tmp/$server-normal-total-time.tmp" ) ms"
				echo
				) &

				sleep 0.5

		fi
	done
	echo
done

wait $(jobs -rp)

#calculating different points in time for statistics
time_one_year_ago=$(( current_time_in_ms - 31557600000 ))
time_one_month_ago=$(( current_time_in_ms - 2629800000 ))
time_one_week_ago=$(( current_time_in_ms - 604800000 ))
time_one_day_ago=$(( current_time_in_ms - 86400000 ))

### set state of dns servers and write it into log files ###

#making changes to html file depending on state of servers
#and chaining parameters of all results for sed command
sed_command_parameter=""
for server in ${dns_servers[*]}; do
	normal_total_time="$( cat "tmp/$server-normal-total-time.tmp" )"
	normal_average_time=$(( normal_total_time / 10 ))
	log_info ${LINENO} "normal_average_time: $normal_average_time, server: $server."
	encrypted_total_time="$( cat "tmp/$server-encrypted-total-time.tmp" )"
	encrypted_average_time=$(( encrypted_total_time / 10 ))
	log_info ${LINENO} "encrypted_average_time: $encrypted_average_time, server: $server."
	normal_state=""
	enrypted_state=""
	normal_errors="$( cat "tmp/$server-normal-errors.tmp" )"
	log_info ${LINENO} "normal_errors: $normal_errors, server: $server."
	encrypted_errors="$( cat "tmp/$server-encrypted-errors.tmp" )"
	log_info ${LINENO} "encrypted_errors: $encrypted_errors, server: $server."

	#setting state depending on errors and average time
	if [[ $normal_errors -ge 5 ]]; then
		normal_state=offline
	else
		if [[ $normal_average_time -gt 200 ]]; then
			normal_state=slow
		else
			normal_state=online
		fi
	fi

	if [[ $encrypted_errors -ge 5 ]]; then
		enrypted_state=offline
	else
		if [[ $encrypted_average_time -gt 200 ]]; then
			enrypted_state=slow
		else
			enrypted_state=online
		fi
	fi

	echo "$server"
	echo "Errors (unencrypted): $normal_errors"
	echo "Errors (encrypted): $encrypted_errors"
	echo Total time: $(( $normal_total_time + $encrypted_total_time)) ms
	echo "Total time (unencrypted): $normal_total_time ms"
	echo "Total time (encrypted): $encrypted_total_time ms"
	echo "Average time (unencrypted): $normal_average_time ms"
	echo "Average time (encrypted): $encrypted_average_time ms"
	echo

	#chaining parameters so the sed command has only to be called once
	sed_command_parameter+="s,$server-state,$normal_state,g;"
	sed_command_parameter+="s,$server-encrypted-state,$enrypted_state,g;"

	#write state into log file
	echo $normal_state $current_time_in_ms >> "log/$server-unencrypted.log"
	echo $enrypted_state $current_time_in_ms >> "log/$server-encrypted.log"

	#if there is no file with the start of measurement create one
	if ! test -f "log/time-of-first-measurement/$server-unencrypted.log"; then
		echo $current_time_in_ms > "log/time-of-first-measurement/$server-unencrypted.log"
	fi
	if ! test -f "log/time-of-first-measurement/$server-encrypted.log"; then
		echo $current_time_in_ms > "log/time-of-first-measurement/$server-encrypted.log"
	fi

done

rm -r "tmp"


### test sites and write state into log files ###

for site in ${sites_to_check[*]}; do
	state=""
	if [ $(( "$( wget -q --spider "$site" )" )) -eq 0 ]; then
		state=online
	else
		state=offline
	fi
	sed_command_parameter+="s,$site-state,$state,g;"

	#write state into log file
	cut=$(echo "$site" | sed "s,.*\/\/,,g;s,\/.*,,g")
	echo $state $current_time_in_ms >> "log/$cut.log"

	#if there is no file with the start of measurement create one
	if ! test -f "log/time-of-first-measurement/$cut.log"; then
		echo $current_time_in_ms > "log/time-of-first-measurement/$cut.log"
	fi
done

create_HTML_elements_for_service () {
	service=$1	#$server-unencrypted / $server-encrypted / $site
	kind_of_service=$2	#"site" / "server"
	log_info ${LINENO} "create_HTML_elements_for_service for service: $service."
	site=""
	if [[ "$kind_of_service" == "site" ]]; then
		site=$service
		service=$(echo "$service" | sed "s,.*\/\/,,g;s,\/.*,,g")
		echo "$service"
	fi
	html_log=""
	tmp_html_log=""
	count=1

	time_of_last_entry=""
	time_of_last_state=""
	time_of_current_state=""
	state_of_last_entry=""
	state_of_current_entry=""
	last_state_before_change=""
	passed_time=""

	time_overall=0
	online_time_overall=0
	online_time_last_year=0
	online_time_last_month=0
	online_time_last_week=0
	online_time_last_day=0

	time_of_first_measurement="$( cat "log/time-of-first-measurement/$service.log" )"

	number_of_lines=$(wc -l < "log/$service.log")

	if [[ number_of_lines -gt 1 ]]; then
		while read -r line
		do
			state_of_current_entry=$(echo "$line" | sed "s, .*,,g")

			#testing if there was a change of the state
			if [[ "$state_of_current_entry" != "$last_state_before_change" && "$last_state_before_change" != "" ]]; then
				echo "last_state_before_change: $last_state_before_change"
				time_of_last_state=$time_of_current_state
				html_log+="$tmp_html_log"
				tmp_html_log=""
			fi
			echo "time_of_last_state= $time_of_last_state"
			time_of_current_state=$(echo "$line" | sed "s,.* ,,g")
			echo "time_of_current_state= $time_of_current_state"
			last_state_before_change=$state_of_current_entry

			#checking if entry is "too old"
			if [[ $(( current_time_in_ms - time_of_current_state )) -gt $delete_log_entries_after_milliseconds && delete_log_entries_after_milliseconds -ne 0 ]]; then
				lines_to_delete=$count
				echo "$time_of_current_state" > "log/time-of-first-measurement/$service.log"
				time_of_first_measurement=$time_of_current_state
			else
				#Calculating online time for statistics

				#Determine start and end point of time frame
				first_point_in_time=""
				last_point_in_time=""
				if [[ "$time_of_last_entry" == "" && time_of_first_measurement -ne time_of_current_state ]]; then
					first_point_in_time=$time_of_first_measurement
					last_point_in_time=$time_of_current_state
				else
					first_point_in_time=$time_of_last_entry
					last_point_in_time=$time_of_current_state
				fi

				if (( $(echo "$first_point_in_time > $last_point_in_time" | bc) )); then
					log_warn ${LINENO} "First point of time smaller than last point in time (Time in milliseconds since unix epoch). Potential problem: The log entries are messed up. Service $service - line $count in log file."
					(( count++ ))
					continue
				fi

				if [[ "$first_point_in_time" != "" && "$last_point_in_time" != "" ]]; then
					time_difference=$(( last_point_in_time - first_point_in_time ))
					(( time_overall += time_difference ))
				else
					log_error ${LINENO} "First or last point in time is empty."
				fi

				if [[ "$first_point_in_time" != "" && "$last_point_in_time" != "" && "$state_of_current_entry" != offline ]]; then
					#calculating overall online time
					time_difference=$(( last_point_in_time - first_point_in_time ))
					(( online_time_overall += time_difference ))
					additional_online_time_last_year=$(calculate_online_time_in_time_frame first_point_in_time last_point_in_time time_one_year_ago)
					online_time_last_year=$( echo "$additional_online_time_last_year+$online_time_last_year" | bc)
					additional_online_time_last_month=$(calculate_online_time_in_time_frame first_point_in_time last_point_in_time time_one_month_ago)
					online_time_last_month=$( echo "$additional_online_time_last_month+$online_time_last_month" | bc)
					additional_online_time_last_week=$(calculate_online_time_in_time_frame first_point_in_time last_point_in_time time_one_week_ago)
					online_time_last_week=$( echo "$additional_online_time_last_week+$online_time_last_week" | bc)
					additional_online_time_last_day=$(calculate_online_time_in_time_frame first_point_in_time last_point_in_time time_one_day_ago)
					online_time_last_day=$( echo "$additional_online_time_last_day+$online_time_last_day" | bc)
				fi

			fi

			#calculating passed time for log entries on the final site
			if [[ ! "$time_of_last_state" ]]; then
				passed_time=$(( time_of_current_state - time_of_first_measurement ))
			else
				passed_time=$(( time_of_current_state - time_of_last_state ))
			fi


			if [[ passed_time -gt 0 ]]; then
				#calculating days
				days=$(( $passed_time / 86400000 ))
				milliseconds=$(( $passed_time % 86400000 ))

				#calculating hours
				hours=$(( $milliseconds / 3600000 ))
				milliseconds=$(( $milliseconds % 3600000 ))

				#calculating minutes
				minutes=$(( $milliseconds / 60000 ))
				milliseconds=$(( $milliseconds % 60000 ))

				#calculating seconds
				seconds=$(( $milliseconds / 1000 ))
				milliseconds=$(( $milliseconds % 1000 ))


				#checking since when the current state is active and set class for html file
				html_class=""
				period=$(( current_time_in_ms - time_of_current_state ))
				if [[ period -lt 86400000 ]]; then
					html_class="period-day"
				elif [[ period -lt 604800000 ]]; then
					html_class="period-week"
				elif [[ period -lt 2592000000 ]]; then
					html_class="period-month"
				fi

				#setting the appropriate icon for the current line in the log
				src=""
				if [[ "$state_of_current_entry" == "online" ]]; then
					src="images/check_mark.svg"
				elif [[ "$state_of_current_entry" == "slow" ]]; then
					src="images/rectangular_horizontal.svg"
				else
					src="images/x.svg"
				fi

				time_of_current_state_in_seconds=$(( time_of_current_state / 1000 ))
				millisecondsAsDate=$(date -d @$time_of_current_state_in_seconds "+%Y-%m-%d %H:%M:%S")

				state_displayed_on_site=$state_of_current_entry
				if [[ "$state_displayed_on_site" == "slow" ]]; then
					state_displayed_on_site="langsam"
				fi

				if [[ "$time_of_last_state" != "" ]]; then
					tmp_html_log="<div class=\"$html_class log-entry $state_of_current_entry\"><div class=\"icon-container\"><img class=\"icon log-entry-icon\" src=\"$src\"></div><div class=\"log-entry-text\"><div class=\"log-entry-description\">${state_displayed_on_site^} seit $days Tagen $hours Stunden $minutes Minuten $seconds Sekunden</div><div class=\"log-entry-time-stamp\">$millisecondsAsDate</div></div></div>"
				else
					tmp_html_log="<div class=\"$html_class log-entry $state_of_current_entry\"><div class=\"icon-container\"><img class=\"icon log-entry-icon\" src=\"$src\"></div><div class=\"log-entry-text\"><div class=\"log-entry-description\">${state_displayed_on_site^} seit mindestens $days Tagen $hours Stunden $minutes Minuten $seconds Sekunden</div><div class=\"log-entry-time-stamp\">$millisecondsAsDate</div></div></div>"
				fi

				#deleting parts with value 0 and correct text if value equals 1
				if [[ days -eq 0 ]]; then
					tmp_html_log=${tmp_html_log/" 0 Tagen"/""}
				elif [[ days -eq 1 ]]; then
					tmp_html_log=${tmp_html_log/"Tagen"/"Tag"}
				fi
				if [[ hours -eq 0 ]]; then
					tmp_html_log=${tmp_html_log/" 0 Stunden"/""}
				elif [[ hours -eq 1 ]]; then
					tmp_html_log=${tmp_html_log/"Stunden"/"Stunde"}
				fi
				if [[ minutes -eq 0 ]]; then
					tmp_html_log=${tmp_html_log/" 0 Minuten"/""}
				elif [[ minutes -eq 1 ]]; then
					tmp_html_log=${tmp_html_log/"Minuten"/"Minute"}
				fi
				if [[ seconds -eq 1 ]]; then
					tmp_html_log=${tmp_html_log/"Sekunden"/"Sekunde"}
				fi
			fi

			if [[ count -eq number_of_lines ]]; then
				html_log+="$tmp_html_log"
			fi

			state_of_last_entry=$last_state_before_change
			time_of_last_entry=$time_of_current_state
			(( count++ ))
		done < "log/$service.log"
	else
		html_log="<div>Keine Daten verfuegbar!</div>"
	fi
	if [[ "$kind_of_service" == "site" ]]; then
		sed -i "s,$site-log,$html_log,g" state-site-new.html
	else
		sed -i "s,$service-log,$html_log,g" state-site-new.html
	fi

	#delete entries that are "too old"
	if [[ lines_to_delete -gt 0 && delete_log_entries_after_milliseconds -ne 0 ]]; then
		(( lines_to_delete++ ))
		tail -n +$lines_to_delete "log/$service.log" > "log/$service-backup.log"
		mv "log/$service-backup.log" "log/$service.log"
	fi

	echo -e "time_overall\t\t\t$time_overall"
	echo -e "online_time_overall\t\t$online_time_overall"
	echo -e "online_time_last_year\t\t$online_time_last_year"
	echo -e "online_time_last_month\t\t$online_time_last_month"
	echo -e "online_time_last_week\t\t$online_time_last_week"
	echo -e "online_time_last_day\t\t$online_time_last_day"

	echo
	#calculating online time in percentage
	if [[ time_overall -ne 0 ]]; then
		percentage_overall=$(echo "scale=2; $online_time_overall*100/$time_overall" | bc)
		echo "overall online time in percentage: $percentage_overall"
	fi

	log_info ${LINENO} "time_overall: $time_overall."

	log_info ${LINENO} "online_time_last_year: $online_time_last_year."
	percentage_year=$(calculate_online_time 31557600000 $online_time_last_year $time_overall)
	echo "online time in percentage of last year: $percentage_year"
	log_info ${LINENO} "percentage_year: $percentage_year."

	log_info ${LINENO} "online_time_last_month: $online_time_last_month."
	percentage_month=$(calculate_online_time 2629800000 $online_time_last_month $time_overall)
	echo "online time in percentage of last month: $percentage_month"
	log_info ${LINENO} "percentage_month: $percentage_month."

	log_info ${LINENO} "online_time_last_week: $online_time_last_week."
	percentage_week=$(calculate_online_time 604800000 $online_time_last_week $time_overall)
	echo "online time in percentage of last week: $percentage_week"
	log_info ${LINENO} "percentage_week: $percentage_week."

	log_info ${LINENO} "online_time_last_day: $online_time_last_day."
	percentage_day=$(calculate_online_time 86400000 $online_time_last_day $time_overall)
	echo "online time in percentage of last day: $percentage_day"
	log_info ${LINENO} "percentage_day: $percentage_day."

	if [[ $(echo "$percentage_overall > -1" | bc) -eq 1 ]]; then
		html_overall=$(create_html_element_for_uptime "$percentage_overall" "$kind_of_service" "$service" "$server" "$site")
	fi
	if [[ $(echo "$percentage_year > -1" | bc) -eq 1 ]]; then
		html_year=$(create_html_element_for_uptime "$percentage_year" "$kind_of_service" "$service" "$server" "$site")
	fi
	if [[ $(echo "$percentage_month > -1" | bc) -eq 1 ]]; then
		html_month=$(create_html_element_for_uptime "$percentage_month" "$kind_of_service" "$service" "$server" "$site")
	fi
	if [[ $(echo "$percentage_week > -1" | bc) -eq 1 ]]; then
		html_week=$(create_html_element_for_uptime "$percentage_week" "$kind_of_service" "$service" "$server" "$site")
	fi
	if [[ $(echo "$percentage_day > -1" | bc) -eq 1 ]]; then
		html_day=$(create_html_element_for_uptime "$percentage_day" "$kind_of_service" "$service" "$server" "$site")
	fi
	if [[ "$kind_of_service" == "site" ]]; then
		sed_command_parameter+="s,$site-bar-day,$html_day,g;s,$site-bar-week,$html_week,g;s,$site-bar-month,$html_month,g;s,$site-bar-year,$html_year,g;s,$site-bar-overall,$html_overall,g;"
	else
		sed_command_parameter+="s,$service-bar-day,$html_day,g;s,$service-bar-week,$html_week,g;s,$service-bar-month,$html_month,g;s,$service-bar-year,$html_year,g;s,$service-bar-overall,$html_overall,g;"
	fi
	echo
	echo
}

### create html elements for the logs of all sites ###

for site in ${sites_to_check[*]}; do
	echo "$site"
	create_HTML_elements_for_service "$site" "site"
done


### create html elements for the logs of all dns servers ###

for server in ${dns_servers[*]}; do
	if [[ $server != "dns3.digitalcourage.de" ]]; then
		echo "$server unencrypted"
		create_HTML_elements_for_service "$server-unencrypted" "server"
	fi

	echo "$server encrypted"
	create_HTML_elements_for_service "$server-encrypted" "server"
done

stamp=$(date "+%Y-%m-%d %H:%M:%S")
log_info ${LINENO} "Stamp: $stamp"
sed_command_parameter+="s,state-date,$stamp,g;"
start_of_sed=$(date '+%s%3N')
sed -i "$sed_command_parameter" state-site-new.html
end_of_sed=$(date '+%s%3N')
duration_of_sed=$(echo "$end_of_sed - $start_of_sed" | bc)
log_info ${LINENO} "Duration of sed command: $duration_of_sed ms."

mv "state-site-new.html" "state-site.html"

runtime_stop=$(( $(date '+%s%N')/1000000 ))
runtime=$(( $runtime_stop - $current_time_in_ms))
echo "Overall runtime: $runtime ms"
log_info ${LINENO} "Duration of script: $runtime ms"
exit 0
