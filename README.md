# Status website for Digitalcourage

This project is used to create a website that shows the state of different services belonging to Digitalcourage (DNS-servers and different websites).

There is a state-template.html which is the scaffold for the website. The script state-check.py (previous: state-check.sh) is used to fill the scaffold with data about the services.

## Dependencies

### Python
To run state-check.sh it is necessary to install the following packages:
* dnspython
* requests

You can install them by running

`apt install python3-dnspython python3-requests`

or

`pip install dnspython requests`

### Bash (Legacy)
To run state-check.sh it is necessary to install the following packages:
* knot-dnsutils
* bc
