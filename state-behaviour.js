let numberOfVisibleLogEntries = 0;
let selectedPeriodForLogEntries = "";

window.onload = function () {
    resetNumberOfVisibleLogEntries();
    displaySelectedLog();
    selectPeriodForUptime();
}

window.onscroll = function(){
    /* Detect how far the user did scroll and display the button for scrolling to the top if the user is not at the top */
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    if(window.scrollY > (vh *1,25)){
        if(document.getElementsByClassName("scroll-up-button")[0]){
            document.getElementsByClassName("scroll-up-button")[0].classList.add("scroll-up-button-visible");
        }
    }else{
        if(document.getElementsByClassName("scroll-up-button")[0]){
            document.getElementsByClassName("scroll-up-button")[0].classList.remove("scroll-up-button-visible");
        }
    }
}

function resetNumberOfVisibleLogEntries() {
    numberOfVisibleLogEntries = 5;
}

function adjustNumberOfVisibleLogEntries() {
    const visibleLogsContainer = document.querySelectorAll(".log-content:not(.invisible)");
    const allLogs = visibleLogsContainer[0].children
    let count = 1;
    for (let a = allLogs.length - 1; a >= 0; a--) {
        const entry = allLogs[a];
        if (count > numberOfVisibleLogEntries) {
            entry.classList.add('invisible');
        } else {
            entry.classList.remove('invisible');
            count++;
        }
    }

    let query = [];
    if (selectedPeriodForLogEntries == "period-day" || selectedPeriodForLogEntries == "period-week" || selectedPeriodForLogEntries == "period-month") {
        query.push(".period-day")
    }
    if (selectedPeriodForLogEntries == "period-week" || selectedPeriodForLogEntries == "period-month") {
        query.push(".period-week")
    }
    if (selectedPeriodForLogEntries == "period-month") {
        query.push(".period-month")
    }
    const visibleLogEntries = visibleLogsContainer[0].querySelectorAll(query.toString());
    if (numberOfVisibleLogEntries >= visibleLogEntries.length) {
        document.getElementById('load-logs-button').classList.add('invisible');
    } else {
        document.getElementById('load-logs-button').classList.remove('invisible');
    }
}

function showMoreLogEntries() {
    numberOfVisibleLogEntries += 5;
    adjustNumberOfVisibleLogEntries();
}

function displaySelectedLog() {
    resetNumberOfVisibleLogEntries();
    let selectedService = document.getElementById("service-selection").value;
    let kindOfService = document.getElementById("service-selection").options[document.getElementById("service-selection").selectedIndex].parentElement.label;
    if(kindOfService === "Websites" || selectedService === 'dns3'){
        deactivateEncryptionSelection();
    }else{
        activateEncryptionSelection();
    }
    let stateOptions = document.getElementsByClassName("state-selection");
    let selectedState;
    for (let a = 0; a < stateOptions.length; a++) {
        if (stateOptions[a].classList.contains("custom-radio-circle-activated")) {
            selectedState = stateOptions[a].id.replace('state-selection-', '');
        }
    }
    if(selectedService === 'dns3'){
        selectEncryptedRadioButton();
        selectedState = 'encrypted';
    }
    let periodOptions = document.getElementsByClassName("period-selection");
    let selectedPeriod;
    for (let a = 0; a < periodOptions.length; a++) {
        if (periodOptions[a].classList.contains("custom-radio-circle-activated")) {
            selectedPeriod = periodOptions[a].id.replace('period-selection-', '');
        }
    }
    let logContainer = document.getElementById("log-content-container");
    logContainer.classList.remove("period-day");
    logContainer.classList.remove("period-week");
    logContainer.classList.remove("period-month");
    logContainer.classList.add(("period-" + selectedPeriod));
    selectedPeriodForLogEntries = "period-" + selectedPeriod

    /* Make the correct log visible */
    const serviceLogs = document.getElementsByClassName("log-content");
    for (let a = 0; a < serviceLogs.length; a++) {
        if((kindOfService === "Websites" && serviceLogs[a].id === (selectedService + "-log-content")) || (kindOfService === "DNS-Server" && serviceLogs[a].id === (selectedService + "-" + selectedState + "-log-content"))){
            serviceLogs[a].classList.remove("invisible");
        } else {
            serviceLogs[a].classList.add("invisible");
        }
    }
    adjustNumberOfVisibleLogEntries();
}

function selectOption(option, selection) {
    if (selection === "period" || selection === "state" && !document.getElementsByClassName("state-selection")[0].parentElement.classList.contains("deactivated")) {
        let radioCircles = document.getElementsByClassName(selection + "-selection");
        for (let a = 0; a < radioCircles.length; a++) {
            if (radioCircles[a].id === (selection + "-selection-" + option)) {
                radioCircles[a].classList.add("custom-radio-circle-activated");
            } else {
                radioCircles[a].classList.remove("custom-radio-circle-activated");
            }
        }
        displaySelectedLog();
    }
}

function adjustEncryptionSelection(disabled) {
    let radioCircles = document.getElementsByClassName("state-selection");
    for (let a = 0; a < radioCircles.length; a++) {
        if (disabled === true) {
            radioCircles[a].parentElement.classList.add("deactivated");
        } else {
            radioCircles[a].parentElement.classList.remove("deactivated");
        }
    }
}

function activateEncryptionSelection() {
    adjustEncryptionSelection(false);

}

function deactivateEncryptionSelection() {
    adjustEncryptionSelection(true);
}

function selectEncryptedRadioButton(){
    const radioButtonUnencrypted = document.getElementById('state-selection-unencrypted');
    radioButtonUnencrypted.classList.remove('custom-radio-circle-activated');
    const radioButtonEncrypted = document.getElementById('state-selection-encrypted');
    radioButtonEncrypted.classList.add('custom-radio-circle-activated');
}

function selectPeriodForUptime() {
    let section = document.getElementById("uptime-section");
    let uptimeSelection = document.getElementById("uptime-selection").value;
    section.classList.remove("overall");
    section.classList.remove("year");
    section.classList.remove("month");
    section.classList.remove("week");
    section.classList.remove("day");
    section.classList.add(uptimeSelection);
}
