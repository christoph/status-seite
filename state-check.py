import datetime
import time
import shutil
import os
import math
from requests import codes, head
from dns import name, query, rcode, rdatatype
from dns.message import make_query
from dns.exception import Timeout

WORKING_DIRECTORY = os.getcwd()

MAX_AGE_OF_LOG_FILES_IN_MS = 365 * 86400000 # (86400000 = 1 day)

timeout_in_sec = 5

sites_to_check_with_dns_server = [
    "example.org",
    "google.com",
    "microsoft.com",
    "ix.de",
    "youtube.com"
]

dns_servers = ["dns2.digitalcourage.de", "dns3.digitalcourage.de"]

sites_to_check = [
    "https://digitalcourage.de/",
    "https://bigbrotherawards.de/",
    "https://digitalcourage.social/",
    "https://digitalcourage.video/",
    "https://nuudel.digitalcourage.de/",
    "https://pad.foebud.org/",
    "https://cryptpad.digitalcourage.de/",
    "https://gitlab.digitalcourage.de/"
]

def get_unix_time_in_ms():
    return round(time.time() * 1000)

def cp(source, destination):
    shutil.copyfile(WORKING_DIRECTORY + "/" + source, WORKING_DIRECTORY + "/" + destination)

def create_dir(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

def ms_to_date(milliseconds: int) -> str:
    seconds = milliseconds / 1000
    millisecondsAsDate = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(seconds))
    return millisecondsAsDate

def replace_in_file(to_replace: str, replacement: str, file: str):
    with open(file, "r") as f:
        content = f.read()
    new_content = content.replace(to_replace, replacement)

    with open(file, "w") as f:
        f.write(new_content)

def remove_lines_in_file(number_of_lines: int, file: str):
    with open(file, 'r') as f:
        data = f.read().splitlines(True)
    with open(file, 'w') as f:
        f.writelines(data[number_of_lines:])

def create_log_file_name(service: str, is_server: bool = False, is_encrypted: bool = False) -> str:
    file_name = "log/" + service
    if is_server:
        if is_encrypted:
            file_name += "-encrypted"
        else:
            file_name += "-unencrypted"
    file_name += ".log"
    return file_name

def check_state_of_server(ip: str, encrypted: bool = False) -> str:
    number_of_errors = 0
    duration_in_ms = 0
    for site in sites_to_check:
        qname = name.from_text(site.replace("https://", "").replace("/", ""))
        answer = None
        start = get_unix_time_in_ms()
        try:
            if encrypted:
                queryMessage = make_query(qname, rdatatype.A)
                answer = query.tls(q = queryMessage, where = ip, timeout = timeout_in_sec)
                end = get_unix_time_in_ms()
                duration_in_ms += end - start
            else:
                queryMessage = make_query(qname, rdatatype.NS)
                answer = query.udp(q = queryMessage, where = ip, timeout = timeout_in_sec)
                end = get_unix_time_in_ms()
                duration_in_ms += end - start
            if answer.rcode() != rcode.NOERROR:
                number_of_errors += 1
        except Timeout as e:
            end = get_unix_time_in_ms()
            duration_in_ms += end - start
            number_of_errors += 1
    duration_average = duration_in_ms / len(sites_to_check)
    state = "online"
    if number_of_errors > 5:
        state = "offline"
    elif duration_average > 200:
        state = "slow"
    return state

def check_state_of_site(url) -> str:
    state = "online"
    r = head(url, allow_redirects=True)
    if r.status_code != codes.ok:
        state = "offline"
    return state

def write_service_state_to_file(
        state: str,
        unix_time_of_check: str,
        sanitized_service: str,
        is_server: bool = False,
        is_encrypted: bool = False
    ):
    file_name = create_log_file_name(sanitized_service, is_server, is_encrypted)
    with open(file_name, "a") as logfile:
        logfile.write(state + " " + str(unix_time_of_check) + "\n")

def insert_state_into_template(state, service, is_encrypted = False):
    string_to_replace = service
    if is_encrypted:
        string_to_replace += "-encrypted"
    string_to_replace += "-state"
    with open("state-site-new.html", "r") as file:
        content = file.read()
        content = content.replace(string_to_replace, state)
    with open("state-site-new.html", "w") as file:
        file.write(content)

def create_html_error_element_for_uptime(sanitized_service: str, is_server: bool = False, is_encrypted: bool = False) -> str:
    return create_html_element_for_uptime(-1, sanitized_service, is_server, is_encrypted)

def create_html_element_for_uptime(percentage: float, sanitized_service: str, is_server: bool = False, is_encrypted: bool = False) -> str:
    create_placeholder = False
    offline_percentage = round(100 - percentage, 2)
    if percentage < 0 or offline_percentage > 100:
        create_placeholder = True

    element = "<div class=\"uptime-bar\""
    if create_placeholder:
        element += "style=\"background-color: #EEEEEE\""
    element += "><div class=\"bar-left\" style=\"width: "
    if create_placeholder:
        element += "0"
    else:
        element += str(percentage)
    element += "%\"></div><div class=\"bar-right\" style=\"width: "
    if create_placeholder:
        element += "0"
    else:
        element += str(offline_percentage)
    element += "%\"></div><div class=\"bar-percentage\">"
    if create_placeholder:
        element += "--"
    else:
        element += str(percentage)
    element += "%</div>"

    if is_server:
        element += "<div class=\"icon-with-description\"><div class=\"icon-container\">"\
            + "<img class=\"icon\" src=\"images/"
        if is_encrypted:
            element+="lock_icon.svg\" alt=\"Icon: Verschlüsselt\" title=\"verschlüsselt\">"
        else:
            element+="lock_open_icon.svg\" alt=\"Icon: Unverschlüsselt\" title=\"unverschlüsselt\">"
        element += "</div><div class=\"icon-description-container\"><div class=\"bar-site\">"\
            + service\
            + "</div><div class=\"icon-description\">"
        if is_encrypted:
            element += "Verschlüsseltes DNS über TLS</div>"
        else:
            element += "Unverschlüsseltes DNS über UDP</div>"

        element+="</div></div>"
    else:
        element+="<div class=\"bar-site\">" + sanitized_service + "</div>"

    element+="</div>"
    return element

def create_html_element_for_history_entry(duration: int, state_of_current_entry: str, time: int, start_of_script: int) -> str:
    element = ""
    try:
        html_class = ""
        if time > start_of_script - 86400000:
            html_class = "period-day"
        elif time > start_of_script - 604800000:
            html_class = "period-week"
        elif time > start_of_script - 2592000000:
            html_class = "period-month"

        if duration > 0:
            days = math.floor(duration / 86400000)
            milliseconds = duration % 86400000

            hours = math.floor(milliseconds / 3600000)
            milliseconds = milliseconds % 3600000

            minutes = math.floor(milliseconds / 60000)
            milliseconds = milliseconds % 60000

            seconds = math.floor(milliseconds / 1000)
            milliseconds = milliseconds % 1000

            src=""
            if state_of_current_entry == "online":
                src = "images/check_mark.svg"
            elif state_of_current_entry == "slow":
                src = "images/rectangular_horizontal.svg"
            else:
                src = "images/x.svg"

            millisecondsAsDate = ms_to_date(time)

            state_displayed_on_site = state_of_current_entry
            if state_displayed_on_site == "slow":
                state_displayed_on_site="langsam"

            element = "<div class=\""\
                    + html_class\
                    + " log-entry "\
                    + state_of_current_entry\
                    + "\"><div class=\"icon-container\"><img class=\"icon log-entry-icon\" src=\""\
                    + src\
                    + "\"></div><div class=\"log-entry-text\"><div class=\"log-entry-description\">"\
                    + state_displayed_on_site.title()\
                    + " seit "

            if days > 0:
                if days == 1:
                    element += "1 Tag "
                else:
                    element += str(days) + " Tagen "

            if hours > 0:
                if hours == 1:
                    element += "1 Stunde "
                else:
                    element += str(hours) + " Stunden "

            if minutes > 0:
                if minutes == 1:
                    element += "1 Minute "
                else:
                    element += str(minutes) + " Minuten "

            if seconds > 0:
                if seconds == 1:
                    element += "1 Sekunde"
                else:
                    element += str(seconds) + " Sekunden"
            element += "</div><div class=\"log-entry-time-stamp\">"\
                    + millisecondsAsDate\
                    + "</div></div></div>"
    except Exception as e:
        element = "<div class=\""
        if html_class != "":
            element += html_class
        element += " log-entry error"\
            + "\"><div class=\"icon-container\"><img class=\"icon log-entry-icon\" src=\""\
            + "images/x.svg"\
            + "\"></div><div class=\"log-entry-text\"><div class=\"log-entry-description\">"\
            + "Fehler"\
            + "</div><div class=\"log-entry-time-stamp\">"
        if millisecondsAsDate != None:
            element += millisecondsAsDate
        else:
            element += "Fehler"
        element += "</div></div></div>"
    return element

def calculate_uptime_in_time_frame(start_of_first_frame: int, end_of_first_frame: int, start_of_second_frame: int) -> int:
    if start_of_first_frame < 0  or end_of_first_frame < 0 or start_of_second_frame < 0:
        print("One of the values is less than 0.")
        return
    elif end_of_first_frame < start_of_first_frame:
        print("End of first frame less than start of first frame.")
        return
    elif end_of_first_frame < start_of_second_frame:
        return 0
    else:
        uptime = end_of_first_frame - start_of_first_frame
        time_before_second_frame = 0
        #Dismiss the time before the time frame that interests us
        if start_of_first_frame < start_of_second_frame:
            time_before_second_frame = start_of_second_frame - start_of_first_frame

        added_time = uptime - time_before_second_frame
        return added_time

def calculate_uptime(duration_of_period: int, measured_uptime: int, duration_since_start: int):
    if measured_uptime == 0 or duration_since_start == 0:
        return
    if duration_since_start > duration_of_period:
        return round(measured_uptime * 100 / duration_of_period, 2)
    else:
        return round(measured_uptime * 100 / duration_since_start, 2)

def handle_results(start_of_script: int, service: str, is_server: bool = False, is_encrypted: bool = False):
    sanitized_service = service.replace("https://", "").replace("/", "")
    log_file_name = create_log_file_name(sanitized_service, is_server, is_encrypted)
    number_of_lines = 0
    lines_to_delete = 0

    time_overall = 0
    online_time_overall = 0
    online_time_last_year = 0
    online_time_last_month = 0
    online_time_last_week = 0
    online_time_last_day = 0

    time_of_previous_state = None

    time_of_previous_entry = None
    state_of_previous_entry = None

    time_of_current_entry = None
    state_of_current_entry = None

    time_one_year_ago = start_of_script - 31557600000
    time_one_month_ago = start_of_script - 2629800000
    time_one_week_ago = start_of_script - 604800000
    time_one_day_ago = start_of_script - 86400000

    with open(log_file_name, "r") as file:
        for count, line in enumerate(file):
            pass
        number_of_lines = count + 1
    if number_of_lines > 1:
        html_log=""
        tmp_html_log=""
        count = 1

        with open(log_file_name, "r") as file:
            while count <= number_of_lines:
                entry = file.readline()
                splitted_entry = entry.split(" ")
                state_of_current_entry = splitted_entry[0]
                time_of_current_entry = int(splitted_entry[1])

                if start_of_script - time_of_current_entry > MAX_AGE_OF_LOG_FILES_IN_MS and MAX_AGE_OF_LOG_FILES_IN_MS > 0:
                    lines_to_delete = count

                if count == 1:
                    time_of_previous_state = time_of_current_entry
                    time_of_previous_entry = time_of_current_entry
                    state_of_previous_entry = state_of_current_entry
                    if count == number_of_lines:
                        html_log = "<div>Nicht genügend Daten verfuegbar!</div>"
                    count += 1
                    continue

                if state_of_current_entry != state_of_previous_entry or count == number_of_lines:
                    first_point_in_time = time_of_previous_state
                    last_point_in_time = time_of_previous_entry
                    time_difference = last_point_in_time - first_point_in_time

                    if count != number_of_lines:
                        tmp_html_log = create_html_element_for_history_entry(time_difference, state_of_previous_entry, time_of_previous_entry, start_of_script)
                    else:
                        tmp_html_log = create_html_element_for_history_entry(time_difference, state_of_current_entry, time_of_current_entry, start_of_script)

                    time_of_previous_state = time_of_previous_entry
                    html_log += tmp_html_log

                passed_time_since_previous_entry = time_of_current_entry - time_of_previous_entry
                time_overall += passed_time_since_previous_entry

                if state_of_current_entry != "offline":
                    online_time_overall += passed_time_since_previous_entry
                    first_point_in_time = time_of_previous_entry
                    last_point_in_time = time_of_current_entry

                    additional_online_time_last_year = calculate_uptime_in_time_frame(first_point_in_time, last_point_in_time, time_one_year_ago)
                    online_time_last_year = additional_online_time_last_year + online_time_last_year

                    additional_online_time_last_month = calculate_uptime_in_time_frame(first_point_in_time, last_point_in_time, time_one_month_ago)
                    online_time_last_month = additional_online_time_last_month + online_time_last_month

                    additional_online_time_last_week = calculate_uptime_in_time_frame(first_point_in_time, last_point_in_time, time_one_week_ago)
                    online_time_last_week = additional_online_time_last_week + online_time_last_week

                    additional_online_time_last_day = calculate_uptime_in_time_frame(first_point_in_time, last_point_in_time, time_one_day_ago)
                    online_time_last_day = additional_online_time_last_day + online_time_last_day

                state_of_previous_entry = state_of_current_entry
                time_of_previous_entry = time_of_current_entry
                count += 1
    else:
        html_log = "<div>Keine Daten verfuegbar!</div>"

    first_part_of_replaced_string = service
    encryption_string = ""
    if is_server:
        if is_encrypted:
            encryption_string = "-encrypted"
        else:
            encryption_string = "-unencrypted"
    first_part_of_replaced_string += encryption_string

    replace_in_file(first_part_of_replaced_string + "-log", html_log, "state-site-new.html")

    #delete entries that are "too old"
    if lines_to_delete > 0 and MAX_AGE_OF_LOG_FILES_IN_MS != 0:
        print("delete old lines:", lines_to_delete)
        remove_lines_in_file(lines_to_delete, log_file_name)

    #calculating online time in percentage
    percentage_overall = 0
    if time_overall != 0:
        percentage_overall = round(online_time_overall * 100 / time_overall, 2)

    percentage_year = calculate_uptime(31557600000, online_time_last_year, time_overall)

    percentage_month = calculate_uptime(2629800000, online_time_last_month, time_overall)

    percentage_week = calculate_uptime(604800000, online_time_last_week, time_overall)

    percentage_day = calculate_uptime(86400000, online_time_last_day, time_overall)

    html_overall = ""
    html_year = ""
    html_month = ""
    html_week = ""
    html_day = ""

    if percentage_overall == None:
        create_html_error_element_for_uptime(service, is_server, is_encrypted)
    else:
        html_overall = create_html_element_for_uptime(percentage_overall, service, is_server, is_encrypted)

    if percentage_year == None:
        create_html_error_element_for_uptime(service, is_server, is_encrypted)
    else:
        html_year = create_html_element_for_uptime(percentage_year, service, is_server, is_encrypted)

    if percentage_month == None:
        create_html_error_element_for_uptime(service, is_server, is_encrypted)
    else:
        html_month = create_html_element_for_uptime(percentage_month, service, is_server, is_encrypted)

    if percentage_week == None:
        create_html_error_element_for_uptime(service, is_server, is_encrypted)
    else:
        html_week = create_html_element_for_uptime(percentage_week, service, is_server, is_encrypted)

    if percentage_day == None:
        create_html_error_element_for_uptime(service, is_server, is_encrypted)
    else:
        html_day = create_html_element_for_uptime(percentage_day, service, is_server, is_encrypted)

    replace_in_file(first_part_of_replaced_string + "-bar-day", html_day, "state-site-new.html")
    replace_in_file(first_part_of_replaced_string + "-bar-week", html_week, "state-site-new.html")
    replace_in_file(first_part_of_replaced_string + "-bar-month", html_month, "state-site-new.html")
    replace_in_file(first_part_of_replaced_string + "-bar-year", html_year, "state-site-new.html")
    replace_in_file(first_part_of_replaced_string + "-bar-overall", html_overall, "state-site-new.html")



if __name__ == "__main__":
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_date = now.strftime("%m-%d-%Y")
    start_of_script = get_unix_time_in_ms()

    duration_of_check = 0

    cp("state-template.html", "state-site-new.html")
    create_dir("log")

    service = "dns2.digitalcourage.de"

    state = check_state_of_server("46.182.19.48")
    write_service_state_to_file(state, start_of_script, service, True)
    insert_state_into_template(state, service)
    handle_results(start_of_script, service, is_server = True)

    state = check_state_of_server("46.182.19.48", True)
    write_service_state_to_file(state, start_of_script, "dns2.digitalcourage.de", True, True)
    insert_state_into_template(state, service, True)
    handle_results(start_of_script, service, is_server = True, is_encrypted = True)

    service = "dns3.digitalcourage.de"
    state = check_state_of_server("5.9.164.112", True)
    write_service_state_to_file(state, start_of_script, "dns3.digitalcourage.de", True, True)
    insert_state_into_template(state, service, True)
    handle_results(start_of_script, service, is_server = True, is_encrypted = True)

    for site in sites_to_check:
        sanitized_site = site.replace("https://", "").replace("/", "")
        state = check_state_of_site(site)
        write_service_state_to_file(state, start_of_script, sanitized_site)
        insert_state_into_template(state, site)
        handle_results(start_of_script, site)

    replace_in_file("state-date", ms_to_date(start_of_script), "state-site-new.html")
    cp("state-site-new.html", "state-site.html")
    os.remove("state-site-new.html")

    duration_of_check = get_unix_time_in_ms() - start_of_script
    print("Duration of script:", duration_of_check, "ms.")
