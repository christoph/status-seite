import random

dns_servers = ["dns2.digitalcourage.de", "dns3.digitalcourage.de"]
sites_to_check = ["digitalcourage.de", "bigbrotherawards.de", "digitalcourage.social", "digitalcourage.video", "nuudel.digitalcourage.de","pad.foebud.org", "cryptpad.digitalcourage.de", "gitlab.digitalcourage.de"]
possible_online_state_site = ["online", "offline"]
possible_online_state_server = possible_online_state_site + ["slow"]

def write_log(file_name, service):
    with open(file_name, "a") as logfile:
        start = None

        with open("time-of-first-measurement/" + file_name, "r") as file1:
            start = int(file1.read())

        for a in range(0, 105192):
            state = None
            if service == "site":
                number = random.randrange(0,2)
                state = possible_online_state_site[number]
            else:
                number = random.randrange(0,3)
                state = possible_online_state_server[number]

            # Create an entry for every 5 minutes (300000 milliseconds)
            start += 300000
            logfile.write(state + " " + str(start) + "\n")

for site in sites_to_check:
    write_log(site + ".log", "site")

for server in dns_servers:
    write_log(server + "-encrypted.log", "server")

write_log("dns2.digitalcourage.de-unencrypted.log", "server")
